MAIN=TempSumCont
SRC= \
  TempSumCont.java \
  TempSumContMapper.java \
  TempSumContReducer.java \
  ValorContador.java



CLASSES=$(SRC:.java=.class)
HADOOP_CLASSPATH=$(shell hadoop classpath)


mapReduce: $(MAIN).jar loadInfo
	hadoop jar $(MAIN).jar MaxT/sample.txt MaxT/output

$(MAIN).jar: $(CLASSES:%.class=bin/%.class)
	jar cvfe $@ $(MAIN) -C bin/ .

bin/%.class: src/%.java
	javac -Xlint:deprecation -sourcepath src -cp $(HADOOP_CLASSPATH) -d bin $<

clean:
	$(RM) -r bin/* *.jar
	hadoop fs -rm -R MaxT

showResult:
	hadoop fs -cat MaxT/output/part*

loadInfo:
	hadoop fs -mkdir MaxT
	hadoop fs -put sample.txt MaxT/
