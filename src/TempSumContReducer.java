import java.io.IOException;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class TempSumContReducer
  extends Reducer<Text, ValorContador, Text, ValorContador> {

  private ValorContador pair = new ValorContador();
  
  @Override
  public void reduce(Text key, Iterable<ValorContador> values,
      Context context)
      throws IOException, InterruptedException {
	
	int valor = 0;
	int count = 0;
    
    for (ValorContador value : values) {
      	valor += value.getValor().get();
		count += value.getCount().get();
    }
	pair.set(valor, count);
    context.write(key, pair);
  }
}
