import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class ValorContador implements Writable, WritableComparable<ValorContador> {

    private IntWritable valor;
    private IntWritable count;

    public ValorContador() {
        set(new IntWritable(0), new IntWritable(0));
    }

    public ValorContador(int valor, int count) {
        set(new IntWritable(valor), new IntWritable(count));
    }

    public void set(int val, int cou){
        this.valor.set(val);
        this.count.set(cou);
    }

    public void set(IntWritable valor, IntWritable count) {
        this.valor = valor;
        this.count = count;
    }
  
    @Override
    public void write(DataOutput out) throws IOException {
        valor.write(out);
        count.write(out);
    }


    @Override
    public void readFields(DataInput in) throws IOException {
        valor.readFields(in);
        count.readFields(in);
    }

    @Override
    public int compareTo(ValorContador other) {
        int compareVal = this.valor.compareTo(other.getValor());
        if (compareVal != 0) {
            return compareVal;
        }
        return this.count.compareTo(other.getCount());
    }

@Override
    public String toString() {
	return "ValorContador{" +
	        " Suma Temperaturas =" + valor +
	        ", Numero Temperaturas=" + count +
	        '}';
    }

    public IntWritable getValor() {
        return valor;
    }

    public IntWritable getCount() {
        return count;
    } 
}
